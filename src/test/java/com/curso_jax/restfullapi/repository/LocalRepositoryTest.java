package com.curso_jax.restfullapi.repository;

import com.curso_jax.restfullapi.entity.Local;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
class LocalRepositoryTest {

    @Autowired
    LocalRepository localRepository;
    @Autowired
    TestEntityManager testEntityManager;


    @BeforeEach
    void setUp() {
        Local local =
                Local.builder()
                        .name("Supermarket")
                        .floor("Tercer piso")
                        .code("Sup-010")
                        .build();
        testEntityManager.persist(local);
    }

    @Test
    public void findLocalByNameIgnoreCaseFound(){
        Optional<Local> local = localRepository.findByNameIgnoreCase("Supermarket");
        assertEquals(local.get().getName(),  "Supermarket");
        System.out.println("local.get() = " + local.get());
    }

    @Test
    public void findLocalByNameIgnoreCaseNotFound(){
        Optional<Local> local = localRepository.findByNameIgnoreCase("cinema");
        assertEquals(local,Optional.empty());
    }

}