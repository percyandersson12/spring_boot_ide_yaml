package com.curso_jax.restfullapi.service;

import com.curso_jax.restfullapi.entity.Local;
import com.curso_jax.restfullapi.error.LocalNotFoundException;

import javax.swing.text.html.Option;
import java.util.List;
import java.util.Optional;

public interface LocalService  {
    List<Local> findAllLocals();

    Local saveLocal(Local local);
    Local updateLocal(Long id,Local local);
    void  deleteLocal(Long id);

    Optional<Local> findLocalByNameWithJPQL(String name);

    Optional<Local> findByName(String name);

    Optional<Local> findByNameIgnoreCase(String name);

    Local findLocalById (Long id) throws LocalNotFoundException;

}

