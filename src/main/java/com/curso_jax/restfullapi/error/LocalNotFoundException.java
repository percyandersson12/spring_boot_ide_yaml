package com.curso_jax.restfullapi.error;

import java.lang.reflect.Constructor;

public class LocalNotFoundException extends Exception{

    public LocalNotFoundException(String message) {
        super(message);
    }
}
