package com.curso_jax.restfullapi.repository;

import com.curso_jax.restfullapi.entity.Local;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface LocalRepository extends JpaRepository<Local,Long> {
    //Consulta con JPQL
    @Query("SELECT l FROM Local l WHERE l.name = :name")
    Optional<Local> findByNameWithJPQL(String name);

    //Consulta con inversion de Control
    Optional<Local> findByName(String name);

    Optional <Local>  findByNameIgnoreCase(String name);
}
