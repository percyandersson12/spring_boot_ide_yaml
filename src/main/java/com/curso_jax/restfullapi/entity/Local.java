package com.curso_jax.restfullapi.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

@Data
@Entity
@Table(name = "locals")
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Local {
     @Id @GeneratedValue(strategy = GenerationType.AUTO)
     private long id;
     @NotBlank(message = "Please add the name")
     private  String name;
     private  String floor;
     @Length(min = 4, max = 10)
     private  String code;
}
