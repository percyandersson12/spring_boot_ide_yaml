package com.curso_jax.restfullapi.controller;

import com.curso_jax.restfullapi.entity.Local;
import com.curso_jax.restfullapi.error.LocalNotFoundException;
import com.curso_jax.restfullapi.service.LocalService;
import jakarta.persistence.PreUpdate;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.swing.text.html.Option;
import java.util.List;
import java.util.Optional;

@RestController
public class LocalController {
    @Autowired
    LocalService localService;


    @GetMapping("/findAllLocalById/{id}")
    Local findLocalById(@PathVariable Long id) throws LocalNotFoundException {
        return localService.findLocalById(id);
    }

    @GetMapping("/findAllLocals")
    public List<Local> findAllLocals(){
        return localService.findAllLocals();
    }

    @PostMapping ("/saveLocal")
    public Local saveLocal(@Valid @RequestBody Local local){
        return  localService.saveLocal(local);
    }

    @PutMapping ("/UpdateLocal/{id}")
    public Local updateLocal(@PathVariable Long id ,@RequestBody Local local) {
        return localService.updateLocal(id,local);
    }

    @DeleteMapping ("/deletedLocal/{id}")
    public String deleteLocal(@PathVariable Long id){
       localService.deleteLocal(id);
        return "Eliminado correctamente";
    }

    @GetMapping ("/findLocalByNameWithJPQL/{name}")
    Optional<Local> findLocalByNameWithJPQL(@PathVariable String name){
      return localService.findLocalByNameWithJPQL(name);
    }

    @GetMapping ("/findByName/{name}")
    Optional<Local> findByName(@PathVariable String name){
        return localService.findByName(name);
    }

    @GetMapping ("/findByNameIgnoreCase/{name}")
    Optional<Local> findByNameIgnoreCase(@PathVariable String name){
        return localService.findByNameIgnoreCase(name);
    }
}
